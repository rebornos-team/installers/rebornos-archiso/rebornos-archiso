#!/bin/bash

# Made by Fernando "maroto"
# Run anything in the filesystem right before being "mksquashed"

script_path=$(readlink -f ${0%/*})
work_dir=work

# Adapted from AIS. An excellent bit of code!
arch_chroot(){
    arch-chroot $script_path/${work_dir}/airootfs /bin/bash -c "${1}"
}  

do_merge(){

arch_chroot "
cd /home/rebornos/Downloads
rm /home/rebornos/.bashrc
cp bashrc /home/rebornos/.bashrc
locale-gen
"
}

#################################
########## STARTS HERE ##########
#################################

do_merge
