## RebornOS (ISO without installer)

Working. Automatic login, user rebornos, no password, NetworkManager and bluetooth actives by default, GNOME 40 Desktop, dock: dash to panel.

Add apps to dock.

How to clone this repo:

```
git clone git@gitlab.com:rebornos-team/installers/rebornos-archiso/rebornos-archiso.git
```

Dependencies:

```
sudo pacman -S archiso mkinitcpio-archiso git squashfs-tools --needed
```

How to build:

```
sudo ./fix_permissions.sh
sudo ./build.sh -v
```

The installer ISO will be in the out folder (folder that will be created automatically).

